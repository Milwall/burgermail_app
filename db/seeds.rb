# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Product.delete_all
Product.create(title: 'King Burger', description: %{<p>The King Burger is the king of all burgers and will satisfy any hunger</p>}, image_url: "kingburger.jpg", price: 10.00)
Product.create(title: 'The Heart Attack', description: %{<p>The Hearth Attack will leave you floored with its mouth watering and heart stopping flavours</p>}, image_url: 'heartattackburger.jpg', price: 15.00)
Product.create(title: 'The Juicey One', description: %{<p>The Juicey One will leave you salivating at the mouth and never dissapoints</p>}, image_url: 'juiceyburger.jpg', price: 12.00)
Product.create(title: 'Cheesey Death', description: %{<p>The Cheesey Death will literally kill with the amount of cheese (proceed with caution)</p>}, image_url: 'cheeseburger.jpg', price: 11.00)
Product.create(title: 'Kebab Burger', description: %{<p>The Kebab Burger is the trusty old fateful after a few beverages</p>}, image_url: 'kebabburger.jpg', price: 13.00)
Product.create(title: 'Deepfried Burger', description: %{<p>Deepfried Burger is a Scottish classic, get those kilts out for this one</p>}, image_url: 'deepfriedburger.jpg', price: 10.00)
Product.create(title: 'The Big Boy', description: %{<p>The Big Boy is not for the faint hearted and serious belly filler</p>}, image_url: 'bigboyburger.jpg', price: 16.00)
Product.create(title: 'The Gold Burger', description: %{<p>The Gold Burger is the creme del la creme of burgers and is only for the elete diner, come join the club</p>}, image_url: 'goldburger.jpg', price: 1000.00)
User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

10.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end
users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end
